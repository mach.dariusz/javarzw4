import { Component, OnInit } from '@angular/core';
import { EventsService } from './service/events.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Events';
  defaultDescription = 'Lorem ipsum dolor sit amet consectetur adipisicing elit.';
  items: Array<Event>;

  constructor(private eventsService: EventsService) {}

  ngOnInit() {
    console.log('ngOnInit() execute ...');

    this.eventsService.getAllEvents().subscribe(data => {
      console.log(data);
      this.items = data['_embedded'].events;
    });
  }
}

interface Event {
  date: Date;
  name: string;
  description: string;
  imageUrl: string;
}
