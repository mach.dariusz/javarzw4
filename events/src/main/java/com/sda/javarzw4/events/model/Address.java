package com.sda.javarzw4.events.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@Document
public class Address {

    @Id
    private String id;

    private String country;
    private String city;
    private String street;
    private String buildingNumber;
    private String flatNumber;
    private String postalCode;
}
