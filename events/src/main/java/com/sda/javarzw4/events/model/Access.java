package com.sda.javarzw4.events.model;

public enum Access {
    PRIVATE,
    PUBLIC
}
