package com.sda.javarzw4.events.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document
public class Event {

    @Id
    private String id;

    private Date date;
    private String name;
    private String description;
    private Address address;
    private Access access;
    private String companyId; // event organizer
    private String imageUrl;
}
