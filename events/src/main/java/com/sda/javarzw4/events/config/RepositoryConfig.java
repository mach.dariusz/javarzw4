package com.sda.javarzw4.events.config;

import com.sda.javarzw4.events.model.Address;
import com.sda.javarzw4.events.model.Company;
import com.sda.javarzw4.events.model.Event;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class RepositoryConfig implements RepositoryRestConfigurer {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Address.class, Company.class, Event.class);
    }
}
