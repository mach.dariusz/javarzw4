package com.sda.javarzw4.events.repository;

import com.sda.javarzw4.events.model.Company;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "eventOrganizers", path = "eventOrganizers")
public interface CompanyRepository extends MongoRepository<Company, String> {

}
