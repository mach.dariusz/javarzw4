package com.sda.javarzw4.events.repository;

import com.sda.javarzw4.events.model.Address;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AddressRepository extends MongoRepository<Address, String> {

}
